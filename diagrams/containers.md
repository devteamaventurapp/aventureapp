# Diagrama de contenedores

![diagram](https://kroki.io/c4plantuml/svg/eNqNVE2P0zAQvftXDD2l0qon7mzalRbQAt32wDGa2NNi4dpZ2ylUiP-OHYd8NWXJoZ2MPW_evDftvfNofX1S7I3UXNWCYPO22Bjt6adfVXMHKDXZdMS89Ipgf3GeTtBWgZB4tHiCg7GQn0n72lJeVYxtyTqjs6r5wjtYpAQuQvhcS9JQ1o4jIPfyLAUKcnCBqqbQ3JIje0a7WLLUrlibWgu0l6wpKgZFAW7dAOV9brGEXwzC03xAN0j2g8oCqyrUfKUSAk8lOXppdGT12Qj6uI_RjrgsCRS6SPIlcMJIrqUV4Ce4D2UmyqIHf0CPJTqKWJ8u--enGOTqhJw0gtRBrBCHthCGHXZo1brukHHk32jQYhPfE1ch3QhfGRep1sqjCOGohQ2TBZOiRIz9ZsB2pHqPevg6kF82h11uNGF3Xuz6G1OO6dLfTcAjaS7jJuQp6jfBHAIvGq7CjPPp0sT6LzOVwXs20q5E_t0cDjJWN8TWXSJy2L7f3sETWjyTiu_vkvoTd69AJia_qKEJq9UqEIGocaNiN_wVTK_09Giu6VhSZypjPYX8PkUzsrVLO9Ftl7KvCNfVVnJQk28_xFEfjUJ9vC1YX_zfat1ybQz1unut6CP6V4yGK3yr11iA3qpO-JskW6NGv6B_eN_dudVxWjt3Lz73pEX8t_4D7oP6Sw==)

## codigo

```
@startuml
!include C4_Context.puml
!include C4_Container.puml

title System Context diagram for AventureApp

Person(persona, "Persona", "Quien busca actividades y puede reservar")
System_Boundary(busca_actividades, "Busca Actividades") {
     Container(web_app, "Web Application", "NodeJS", "Recibe las busquedas y reservas")
     ContainerDb(db_web_app, "Database", "MySQL", "Almacena informacion de busquedas y personas")
     Container(cache_web_app, "Cache", "Redis", "Almacena los resultados de busquedas recientes")
}
 
Rel(persona, web_app, "use")
Rel(web_app, db_web_app, "use")
Rel_R(web_app, cache_web_app, "use")

Person(agencia, "Agencia", "Quien ofrece actividades")
System_Boundary(ofrece_actividades, "Ofrece actividades") {
   Container(backoffice_app, "Backoffice", "PHP, Laravel", "?")
   ContainerDb(db_backoffice_app, "Database", "MySql", "Almacena...")  
}

Rel(agencia, backoffice_app, "use")
Rel(backoffice_app, db_backoffice_app, "use")

Person(soporte, "Soporte")
System_Boundary(reserva_actividades, "Reserva actividades") {
   Container(reserva_api, "Reserva API", "Golang", "?")
   ContainerDb(db_reserva_app, "Database", "MySql", "Almacena...")  
   Container(backoffice_reserva_app, "Backoffice", "PHP, Laravel", "?")
}

Rel(reserva_api, db_reserva_app, "use")
Rel_R(backoffice_reserva_app, reserva_api, "use")
Rel(soporte, backoffice_reserva_app, "use")

Rel(web_app, backoffice_app, "use")
Rel(web_app, reserva_api, "use")
Rel(backoffice_app, reserva_api, "use")
@enduml
```
