Feature: Pagar actividad

  Scenario: Choose payment method
    Given a list of payment methods
    When user select a payment method
    Then user enters data to proceed to payment
    And a notification arrives to the admin
    And user is automatically directed to thank you page and instructions

