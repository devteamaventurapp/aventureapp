Feature: Reservation activities

  Scenario: cancel booking
    Given you have selected a reservation
    When you click the delete reservation button
    Then you must enter a text with the reason for the cancellation
    Then you will see a notification indicating that an email will be sent to the agency and the people who had made the reservation


