Feature: Offers activities

  Scenario: view activity entered
    Given the agency has filled out the activity form
    Given the email of the agency that is required is validated
    When you click the Submit button
    Then the activity already entered is displayed
    Then It receive an email with the activity information

  Scenario: registration fails
    Given the form contains errors
    When submit button is pressed
    Then it shows the list of errors in the header and in each of the fields
